<?php

// require_once 'autoload.php';

// $controller = new Controller\Controller;

// $controller->handleRequest();

require_once('Model/Item.php');
require_once('Model/ShoppingCart.php');
require_once('Model/FreshItem.php');
require_once('Model/Ticket.php');
require_once('Model/Payable.php');

class Main
{
    public function __construct()
    {
        $this->render();

        $ticket = new Ticket("RGBY17032012 - Walles-France", 9000);
        $payable = new Payable($ticket);

        print_r($payable->label());
        echo('</br>');
        print_r($payable->cost());
        echo('</br>');
        print_r($payable->taxRatePerTenThousand());
    }

    public function render()
    {
        require_once('view/layout.php');
    }
}

$main = new Main();
