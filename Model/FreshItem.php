<?php

class FreshItem extends Item
{
  private string $bestBeforeDate;

  public function __construct($name, $price, $weight, $bestBeforeDate)
  {
    parent::__construct($name, $price, $weight);
    $this->bestBeforeDate = $bestBeforeDate;
  }

  public function __toString()
  {
    return "BBD: " . $this->bestBeforeDate . "&nbsp;" . parent::__toString();
  }
}
