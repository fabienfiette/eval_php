<?php

class Payable
{
  private Ticket $ticket;
  private int $taxe;

  public function __construct($ticket, $taxe = 2500)
  {
    $this->ticket = $ticket;
    $this->taxe = $taxe;
  }

  public function label()
  {
    return $this->ticket->getReference();
  }

  public function cost()
  {
    return $this->ticket->getPrice();
  }

  public function taxRatePerTenThousand()
  {
    return $this->taxe;
  }
}
