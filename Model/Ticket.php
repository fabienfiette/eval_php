<?php

class Ticket
{
  private String $reference;
  private int $price;

  public function __construct($reference, $price)
  {
    $this->reference = $reference;
    $this->price = $price;
  }

  public function getReference()
  {
    return $this->reference;
  }

  public function getPrice()
  {
    return $this->price;
  }
}
