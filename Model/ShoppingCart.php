<?php

class ShoppingCart
{
  private array $array = array();
  private int $id;

  public function __construct()
  {
    if (!isset($GLOBALS['idShoppingCart'])) {
      $GLOBALS['idShoppingCart'] = 1;
      $this->id = $GLOBALS['idShoppingCart'];
    } else {
      $GLOBALS['idShoppingCart'] = $GLOBALS['idShoppingCart'] + 1;
      $this->id = $GLOBALS['idShoppingCart'];
    }
  }

  public function addCart($item)
  {
    if (($this->totalWeight() + $item->getWeight()) < 10000) {
      array_push($this->array, $item);
    } else {
      throw new \RuntimeException("Cannot add this items in cart, weight of cart limited at 10Kg.");
    }
  }

  public function removeCart($item)
  {
    if (($key = array_search($item, $this->array)) !== false) {
      unset($this->array[$key]);
      return true;
    } else {
      return false;
    }
  }

  public function itemCount()
  {
    return count($this->array);
  }

  public function getId()
  {
    return $this->id;
  }

  public function totalPrice()
  {
    $totalPrice = 0;
    foreach ($this->array as $value) {
      $totalPrice += $value->getPrice();
    }
    return $totalPrice;
  }

  public function totalWeight()
  {
    $totalWeight = 0;
    foreach ($this->array as $value) {
      $totalWeight += $value->getWeight();
    }
    return $totalWeight;
  }

  public function __toString()
  {
    $text = "panier " . $this->id . " [" . $this->itemCount() . " article(s) ]</br>";
    foreach ($this->array as $value) {
      $text .= "&nbsp;&nbsp;&nbsp;" . $value->__toString() . "</br>";
    }
    return $text;
  }
}
