<?php

class Item
{
  private string $name;
  private int $price;
  private int $weight;

  public function __construct($name, $price, $weight)
  {
    $this->name = $name;
    $this->price = $price;
    $this->weight = $weight;
  }

  public function getName()
  {
    return $this->name;
  }

  public function getPrice()
  {
    return $this->price;
  }

  public function getWeight()
  {
    return $this->weight;
  }

  public function __toString()
  {
    $newPrice = $this->price / 100;
    return $this->name . ": " . number_format((float)$newPrice, 2, '.', '') . " €";
  }
}
